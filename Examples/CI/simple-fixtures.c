//
// CUnit example created by inb on 24/02/25.
//

#include "simple-fixtures.h"
#include "stdio.h"

/* the most basic kind of fixture, very simple functions used to bracket tests */
static void setup_session(void) {
  fprintf(stderr, "\n\t\t >> Begin Session!\n");
}

static void reset_session(void) {
  fprintf(stderr, "\n\t\t  < End Session.\n");
}

/* expose this for tests as an extern */
CU_Fixture custom_session_fixture = {
  "custom session fixture",
  CU_ScopeSession, // session scoped fixtures are setup on first use, and torn down at the end of all tests
  setup_session,
  reset_session,
};

/* a slightly more complex suite-level fixture, that is setup when first used and cleared at the end of the current
 * suite */
static void begin_suite(void) {
  CU_Suite *cur_suite = CU_get_current_suite();
  fprintf(stderr,"\n\t\t >> Begin fixture for Suite %s\n", cur_suite->pName);
}

static void end_suite(void) {
  CU_Suite *cur_suite = CU_get_current_suite();
  fprintf(stderr,"\n\t\t  < End fixture for Suite %s\n", cur_suite->pName);
}

/*
 * Below is an example of how to use values defined by a fixture and read them from tests.
 */

CU_Fixture custom_suite_fixture = {
  "custom suite fixture",
  CU_ScopeSuite,  // suite scoped fixtures are setup on first use, and reset at the end of the current suite
  begin_suite,
  end_suite,
};

/* simple skipper fixtures */

void skip_on_windows(void) {
#ifdef WIN32
  CU_Test *cur_test = CU_get_current_test();
  fprintf(stderr, "Skipping test '%s' : Non-Windows only\n", cur_test->pName);
  CU_SKIP("not windows");
#endif
}

void skip_on_non_windows(void) {
#ifndef WIN32
  CU_Test *cur_test = CU_get_current_test();
  fprintf(stderr, "Skipping test '%s' : requires Windows\n", cur_test->pName);
  CU_SKIP("windows");
#endif
}

CU_Fixture only_windows_fixture = {
  "only windows",
  CU_ScopeFunction,
  skip_on_non_windows
};

CU_Fixture not_windows_fixture = {
  "not windows",
  CU_ScopeFunction,
  skip_on_windows
};
