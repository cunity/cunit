//
// CUnit example created by inb on 24/02/25.
//

#include "CUnit/CUnit.h"

#ifndef CUNIT_SIMPLE_FIXTURES_H
#define CUNIT_SIMPLE_FIXTURES_H

extern CU_Fixture custom_session_fixture;
extern CU_Fixture custom_suite_fixture;
extern CU_Fixture only_windows_fixture;
extern CU_Fixture not_windows_fixture;

#endif //CUNIT_SIMPLE_FIXTURES_H
